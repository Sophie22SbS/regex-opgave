// JavaScript Document

let haystack = `<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
	form {
    	width: 30em;
    }
    #url {
        width: 90%;
    }
</style>
<script defer src="js/opgave0.js"></script>
<script defer src="js/opgave1.js"></script>
<script defer src="js/opgave2.js"></script>
</head>

<body>
	<h1>Opgave 0</h1>
	<ul>
	<li>http://www.example.com/</li>
	<li>http://www.example.com/hello/there.html</li>
	<li>http://example.com/hello/there.html</li>
	<li>www.example.com/hello/there.html</li>
	<li>https://www.example.com</li>
	</ul>
    <form action='#' method='post'>
        <input type='url' id='url'/>
        <input type='submit' id='subBtn' value='Go!'/>
	<div id="feedback"></div>
	</form>
	
	<h1>Opgave 1</h1>
	<p>Kan ikke få det til at virke.</p>
	<a  href="bar-roev.html">Bar røv</a>
	<a  href="gaet-et-tal.html">Gæt et tal</a>
	<a  href="hangman.html">Hangman</a>
	<a  href="vendespil.html">Vendespil</a>
	<div id="linkText"></div>
	
	<h1>Opgave 2</h1>
	<!--^[a-zA-Z0-9ÆØÅæøå]+$-->
	<img src="img/Udklip.PNG" alt="billede">
	
	<h1>Opgave 3</h1>
	<!--(\w+) for hele ordet-->
	<!--(\w) for hvert bogstav-->
	<!--https://stackoverflow.com/questions/19480916/count-number-of-occurrences-for-each-char-in-a-string-->
</body>
</html>`

function func() {
	let regex = /<a\s*href=[\'"](.+?)[\'"].*?>(.+?)<\/a>/gi;
	let a = haystack.match(regex);	
	a.forEach(item => {
		document.getElementById("linkText").innerHTML += " " + item;
	});
	//for (let elm of a) {
	//linkText.textContent += elm;
    //}
}

func();

/*
let regex = /<a\s*href=[\'"](.+?)[\'"].*?>(.+?)<\/a>/gi;
let a = haystack.match(regex);
console.log(a);

let linkText = document.getElementById("linkText");

function func () {
	for (let elm of a) {
		linkText.textContent += elm;
	}
}

func();
*/


