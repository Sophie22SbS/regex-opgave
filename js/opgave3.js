// JavaScript Document

/*------------Tæller ordene--------------*/

//https://stackoverflow.com/questions/42334084/counting-occurrences-of-a-word-in-a-string-with-javascript-using-match
//Jeg prøvede match, men jeg kunne ikke få det til at virke.

//var textArr = text2.split(' ');
//var arr = [...new Set(text2.split(' '))];
//arr.forEach(v => document.getElementById("feedback2").innerHTML = `count: ${textArr.filter(c => c).length}`);

//let arr = text2.split(' ');
//console.log(arr.length);

let nmlarr = text2.match(/\b[a-zA-Z]+\b/gi);
console.log(`niels: ${nmlarr.length}`);

/*------------Tæller hvert ord--------------*/

var textArr1 = text2.split(' ');
var arr1 = [...new Set(text2.split(' '))];

arr1.forEach(v => console.log(`${v} count: ${textArr1.filter(c => c == v).length}`));
arr1.forEach(v => document.getElementById("feedback3").innerHTML = `${v} count: ${textArr1.filter(c => c == v).length}`);

/*------------Tæller hvert bogstav--------------*/

//https://stackoverflow.com/questions/19480916/count-number-of-occurrences-for-each-char-in-a-string

var counts = {};
var ch, index, len, count;

for (index = 0, len = text2.length; index < len; ++index) {
    ch = text2.charAt(index); 
    count = counts[ch];
    counts[ch] = count ? count + 1 : 1;
};

for (ch in counts) {
    console.log(ch + " count: " + counts[ch]);
	document.getElementById("feedback4").innerHTML = ch + " count: " + counts[ch];
};

