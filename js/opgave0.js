// JavaScript Document

//^(https\:\/\/www\. | http\:\/\/www\. | http\:\/\/ | www\.)(\w*\.\w*)
/*
http://www.example.com/
http://www.example.com/hello/there.html
http://example.com/hello/there.html
www.example.com/hello/there.html
https://www.example.com
*/


'use strict';
function urlInput() {
	let s = document.getElementById("url").value;
	let r = /^(https\:\/\/www\.|http\:\/\/www\.|http\:\/\/|www\.)(\w*\.\w*)/;
	let a = s.match(r);
	console.log(a);
	document.getElementById("feedback").innerHTML = a[2];
};

function submit() {
	document.getElementById("subBtn").addEventListener("click", function(event) {
    event.preventDefault();
	urlInput();	
	});
};


window.addEventListener('load', submit);







